#include "Bezier.h"
#include "FillColor.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
	Midpoint_Line(p1.x, p1.y, p2.x, p2.y, ren);
	Midpoint_Line(p2.x, p2.y, p3.x, p3.y, ren);
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);

	Vector2D p;
	for (float t = 0; t <= 1; t += 0.0005)
	{
		p.x = (1 - t)*(1 - t)*p1.x + 2*(1 - t)*t*p2.x + t*t*p3.x;
		p.y = (1 - t)*(1 - t)*p1.y + 2*(1 - t)*t*p2.y + t*t*p3.y;
		SDL_RenderDrawPoint(ren, int(p.x+0.5), int(p.y+0.5));
	}
	CircleFill(p1.x, p1.y, 5, ren, { 0, 255, 0, 255 });
	CircleFill(p2.x, p2.y, 5, ren, { 0, 255, 0, 255 });
	CircleFill(p3.x, p3.y, 5, ren, { 0, 255, 0, 255 });
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
	Midpoint_Line(p1.x, p1.y, p2.x, p2.y, ren);
	Midpoint_Line(p2.x, p2.y, p3.x, p3.y, ren);
	Midpoint_Line(p3.x, p3.y, p4.x, p4.y, ren);
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);

	Vector2D p;
	for (float t = 0; t <= 1; t += 0.0005)
	{
		p.x = (1 - t)*(1 - t)*(1 - t)*p1.x + 3 * (1 - t)*(1 - t)*t*p2.x + 3 * (1 - t)*t*t*p3.x + t*t*t*p4.x;
		p.y = (1 - t)*(1 - t)*(1 - t)*p1.y + 3 * (1 - t)*(1 - t)*t*p2.y + 3 * (1 - t)*t*t*p3.y + t*t*t*p4.y;
		SDL_RenderDrawPoint(ren, int(p.x + 0.5), int(p.y + 0.5));
	}

	CircleFill(p1.x, p1.y, 5, ren, { 0, 255, 0, 255 });
	CircleFill(p2.x, p2.y, 5, ren, { 0, 255, 0, 255 });
	CircleFill(p3.x, p3.y, 5, ren, { 0, 255, 0, 255 });
	CircleFill(p4.x, p4.y, 5, ren, { 0, 255, 0, 255 });
}


