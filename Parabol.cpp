﻿#include "Parabol.h"
#include <iostream>
using namespace std;

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}

void BresenhamDrawParabolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	if (A < 0) A *= -1;
	int x = 0, y = 0;
	int p = 1 - A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A)
	{
		if (p >= 0) {
			p += 2 * x + 3 - 2 * A;
			y++;
		}
		else {
			p += 2 * x + 3;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	x = A, y = A / 2;
	p = 2 * A - 1;
	Draw2Points(xc, yc, x, y, ren);

	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	//	printf("%d %d", w, h);
	while (x + xc >= 0 && y + yc >= 0 && x + xc <= w && y + yc <= h
		&& -x + xc >= 0 && -x + xc <= w)
	{
		if (p >= 0) {
			p += 4 * A - 4 * x - 4;
			x++;
		}
		else {
			p += 4 * A;
		}
		y++;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParabolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	if (A > 0) A *= -1;
	///Area1
	int x = 0, y = 0;
	int p = -1 - A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= -A)						/// Quên chỗ này
	{
		if (p <= 0) {					///Do A < 0, đổi dấu lại
			p += -2 * x - 3 - 2 * A;
			y--;
		}
		else {
			p += -2 * x - 3;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	///Area 2
	x = -A, y = A / 2;
	p = -2 * A - 1;
	Draw2Points(xc, yc, x, y, ren);

	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	//	printf("%d %d", w, h);
	while (x + xc >= 0 && y + yc >= 0 && x + xc <= w && y + yc <= h
		&& -x + xc >= 0 && -x + xc <= w)
	{
		if (p >= 0) {							///Ko có A*(d1-d2)
			p += -4 * A - 4 * x - 4;
			x++;
		}
		else {
			p += -4 * A;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}
