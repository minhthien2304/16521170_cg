#include "Circle.h"
#include <stdio.h>
void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;
    new_x = xc + x;
    new_y = yc + y;
//	printf("%d %d", new_x, new_y);
    SDL_RenderDrawPoint(ren, new_x, new_y);
	new_x = xc + y;
	new_y = yc + x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	new_x = xc + y;
	new_y = yc - x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	new_x = xc - x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	new_x = xc - y;
	new_y = yc - x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	new_x = xc - y;
	new_y = yc + x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0;
	int y = R;
	int d = 3 - 2 * R;
	Draw8Points(xc, yc, x, y, ren);
	while (x < y)
	{
		if (d < 0) {
			d += 4 * x + 6;
		}
		else {
			d += 4 * (x - y) + 10;
			y--;
		}
		x++;
		Draw8Points(xc, yc, x, y, ren); 
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int p = 1 - R;
	int x = 0;
	int y = R;
	Draw8Points(xc, yc, x, y, ren);
	while (x < y)
	{
		if (p < 0)
		{
			p += 2 * x + 3;
		}
		else 
		{
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		Draw8Points(xc, yc, x, y, ren);
	}
}
