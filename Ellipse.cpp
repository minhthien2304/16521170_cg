#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x = 0, y = b;
	int p = -2 * a*a*b + a*a + 2 * b*b;
	// Area 1
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) <= a*a*a*a)
	{
		if (p <= 0) {
			p += 4 * b*b*x + 6 * b*b;
		}
		else {
			p += 4 * b*b*x + 6 * b*b  - 4 * a*a*y + 4 * a*a;
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	x = a, y = 0;
	p = -2 * b*b*a + b*b + 2 * a*a;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) >= a*a*a*a)
	{
		if (p <= 0) {
			p += 4 * a*a*y + 6 * a*a;
		}
		else {
			p += 4 * a*a*y + 6 * a*a - 4 * b*b*x + 4 * b*b;
			x--;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}

    
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1
	int x = 0, y = b;
	Draw4Points(xc, yc, x, y, ren);
	int p = b*b - a*a*b + a*a / 4; 
	while (x*x*(a*a + b*b) <= a*a*a*a)
	{
		if (p < 0) {
			p += b*b*(2*x + 3);
		}
		else {
			p += b*b*(2 * x + 3) + a*a*(-2 * y + 2);
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
    // Area 2
	x = a, y = 0;
	p = a*a - b*b*a + b*b / 4;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a*a + b*b) >= a*a*a*a)
	{
		if (p < 0) {
			p += a*a*(2 * y + 3);
		}
		else {
			p += a*a*(2 * y + 3) + b*b*(-2 * x + 2);
			x--;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}
}