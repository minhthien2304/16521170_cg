﻿#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include "FillColor.h"
using namespace std;

const int WIDTH = 1000;
const int HEIGHT = 800;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}
	
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE
	
	//Test Tô màu HCN bằng thuật toán Loang
	SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
	Vector2D A(10, 10);
	Vector2D B(20, 10);
	Vector2D C(20, 25);
	Vector2D D(10, 25);
	Vector2D startpoint(15, 20);
	Midpoint_Line(A.x, A.y, B.x, B.y, ren);
	Midpoint_Line(B.x, B.y, C.x, C.y, ren);
	Midpoint_Line(C.x, C.y, D.x, D.y, ren);
	Midpoint_Line(D.x, D.y, A.x, A.y, ren);
	
	SDL_Color fillColor = { 255, 0, 0, 255};
	SDL_Color boundaryColor = { 0, 255, 0, 255};
	
//	BoundaryFill4(win, startpoint, SDL_GetWindowPixelFormat(win), ren, fillColor, boundaryColor);

	////Test Tô màu tam giác theo dòng quét
	//Vector2D E(100, 100);
	//Vector2D F(300, 300);
	//Vector2D G(200, 500);
	//TriangleFill(E, F, G, ren, fillColor);

	////Test tô màu HCN theo dòng quét
	//Vector2D A2(500, 100);
	//Vector2D C2(700, 200);
	//RectangleFill(A2, C2, ren, fillColor);

	////Test tô màu Hình tròn theo dòng quét
	//int xc = 600, yc = 200, R = 100;
	//CircleFill(xc, yc, R, ren, fillColor);

	////Test tô màu HCN giao với Hình tròn
	//Vector2D AA(500, 100);
	//Vector2D CC(700, 200);
	//int xc = 600, yc = 200, R = 100;
	//FillIntersectionRectangleCircle(AA, CC, xc, yc, R, ren, fillColor);

	////Test tô màu Elip giao với Hình tròn
	//int xc1 = 600, yc1 = 200, R1 = 100;
	//int xcE = 600, ycE = 200, a = 200, b = 50;
	//FillIntersectionEllipseCircle(xcE, ycE, a, b, xc1, yc1, R1, ren, fillColor);

	////Test tô màu Hình tròn giao với Hình tròn
	//int xc1 = 600, yc1 = 200, R1 = 100;
	//int xc2 = 500, yc2 = 200, R2 = 100;
	//FillIntersectionTwoCircles(xc1, yc1, R1, xc2, yc2, R2, ren, fillColor);

	////Test Bezier bậc 2
	//Vector2D I(100, 100);
	//Vector2D J(300, 300);
	//Vector2D K(200, 500);
	//DrawCurve2(ren, I, J, K);

	//Test Bezier bậc 2
	Vector2D L(100, 100);
	Vector2D M(300, 300);
	Vector2D N(200, 500);
	Vector2D O(400, 700);
	DrawCurve3(ren, L, M, N, O);

	SDL_RenderPresent(ren);
	SDL_Delay(3000);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;
	int x, y;
	Vector2D init;
	Vector2D AA(100, 300), BB(300, 300), CC(500, 300), DD(700, 300);
	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//If the user has Xed out the window
			if (event.type == SDL_MOUSEBUTTONDOWN) {
				SDL_GetMouseState(&x, &y);
				if ((x - AA.x)*(x - AA.x) + (y - AA.y)*(y - AA.y) <= 25) init = AA; else
				if ((x - BB.x)*(x - BB.x) + (y - BB.y)*(y - BB.y) <= 25) init = BB; else
				if ((x - CC.x)*(x - CC.x) + (y - CC.y)*(y - CC.y) <= 25) init = CC; else
				if ((x - DD.x)*(x - DD.x) + (y - DD.y)*(y - DD.y) <= 25) init = DD;
			}

			if (event.type == SDL_MOUSEMOTION)
			{
				SDL_GetMouseState(&x, &y);
				Vector2D tmp(x, y);
				if (init.x == AA.x && init.y == AA.y)	{ 
					AA = tmp; 
					init = tmp; 
				}
				else if (init.x == BB.x && init.y == BB.y) {
					BB = tmp;
					init = tmp;
				}
				else if (init.x == CC.x && init.y == CC.y) {
					CC = tmp;
					init = tmp;
				}
				else if (init.x == DD.x && init.y == DD.y)	{
					DD = tmp;
					init = tmp;
				}
				SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
				SDL_RenderClear(ren);
				SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
				DrawCurve3(ren, AA, BB, CC, DD);
				SDL_RenderPresent(ren);
			}


			if (event.type == SDL_MOUSEBUTTONUP)
			{
				init = NULL;
			}

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
